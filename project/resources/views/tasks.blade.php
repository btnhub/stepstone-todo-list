<!DOCTYPE html>
<html>
<head>
	<title>To Do List</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
	<h1>To Do List</h1>
	<h2>Create A Task</h2>
	{{-- <form action="{{route('tasks.store')}}" method="post"> --}}
		<input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
		<input type="text" name="details" id="details">	
		<button onclick="saveTask()">Submit Task</button>
	{{-- </form> --}}
	
	
	<h2>Existing Tasks</h2>
	<div id="tasks">
		@forelse ($tasks as $task)
			<div id="task-{{$task->id}}">
				<span id="details-{{$task->id}}" @if ($task->completed)
					style="text-decoration: line-through;"
				@endif>{{$task->details}}</span> 
				@if (!$task->completed)
					<span id="incomplete-options-{{$task->id}}">
						<button onclick="showEdit({{$task->id}})">Edit</button>
						<button id="completed-{{$task->id}}" onclick="markAsCompleted({{$task->id}})">Completed</button>
					</span>
				@endif
				<br>

				<div id="edit-{{$task->id}}" style="display:none">
					<input type="text" name="task_details" id="input-{{$task->id}}" value="{{$task->details}}"> 
					<button onclick="updateTask({{$task->id}})">Update</button>
				</div>
			</div>


			<br>
		@empty
			<div id="no_task"><em>No Tasks</em></div>
		@endforelse	
	</div>
	
</body>
<script>
	function saveTask(){
		if ($('#details').val()) {
			$.ajax({
				type: 'POST',
				url: location.protocol + "//" + location.hostname + 
		      		(location.port && ":" + location.port) + "/" + "tasks",
		      	data: {
		      		_token : $('#token').val(),
		      		details : $('#details').val()
		      	},
		      	success: function(data){
		      		$('#no_task').hide();
		      		$('#tasks').append(data.details + "<br>");
		      		$('#details').val('');
		      	},
		     	error: function(xhr, status, error){
	            	console.log(error);
		        }
			});		
		}
	}
	
	function showEdit(task_id){
		document.getElementById('edit-' + task_id).style.display = "block";
	}

	function updateTask(task_id){
		if ($('#input-' + task_id).val()) {
			$.ajax({
				type: 'PATCH',
				url: location.protocol + "//" + location.hostname + 
		      		(location.port && ":" + location.port) + "/" + "tasks/" + task_id,
		      	data: {
		      		_token : $('#token').val(),
		      		details : $('#input-' + task_id).val()
		      	},
		      	success: function(data){
		      		$('#details-' + task_id).html(data.details);
		      		document.getElementById('edit-' + task_id).style.display = "none";
		      	},
		     	error: function(xhr, status, error){
	            	console.log(error);
		        }
			});		
		}
	}

	function markAsCompleted(task_id){
		if ($('#input-' + task_id).val()) {
			$.ajax({
				type: 'PATCH',
				url: location.protocol + "//" + location.hostname + 
		      		(location.port && ":" + location.port) + "/" + "tasks/" + task_id + "/completed",
		      	data: {
		      		_token : $('#token').val(),
		      	},
		      	success: function(data){
		      		document.getElementById('details-' + task_id).style.textDecoration = "line-through";
		      		document.getElementById('incomplete-options-' + task_id).style.display = "none";
		      		document.getElementById('edit-' + task_id).style.display = "none";
		      	},
		     	error: function(xhr, status, error){
	            	console.log(error);
		        }
			});		
		}
	}
</script>	
</html>

